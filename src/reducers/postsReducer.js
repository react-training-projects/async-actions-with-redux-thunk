import { FETCH } from '../actions';

export default (posts = [], action) => {
  var result;

  switch (action.type) {
    case FETCH:
      result = action.payload;
      break;
    default:
      result = posts;
  }
  
  return result;
};