import { USER } from '../actions';

export default (state = [], action) => {
  var result;

  switch (action.type){
    case USER:
      result = [...state, action.payload];
      break;
    default:
      result = state;
  }

  return result;
};
