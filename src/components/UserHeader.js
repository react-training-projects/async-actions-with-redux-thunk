import React from 'react';
import { connect } from 'react-redux';

class UserHeader extends React.Component {
  state = {};

  render() {    
    const name = this.props.user ? this.props.user.name : '?';

    return (
      <div className="user-header header">
        {name}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const user = state.users.find((user) => 
    user.id === ownProps.userId
  );

  return { user: user };
}

export default connect(mapStateToProps)(UserHeader);
