/*
  actions
*/
import _ from 'lodash';

import jsonPlaceholder from '../apis/jsonPlaceholder';

export const FETCH = 'FETCH_POSTS';
export const USER = 'FETCH_USER';

export const fetchPostsAndUsers = () => async (dispatch, getState) => {
  await dispatch(fetchPosts());

  _.chain(getState().posts)
    .map('userId')
    .uniq()
    .forEach(id => dispatch(fetchUser(id)))
    .value();
};

export const fetchPosts = () => async (dispatch) => {
  const response = await jsonPlaceholder.get('/posts');
  const action = {
    type: FETCH,
    payload: response.data
  };

  dispatch(action);
};

export const fetchUser = (id) => async (dispatch) => {
  const response = await jsonPlaceholder.get(`/users/${id}`);
  const action = {
    type: USER,
    payload: response.data
  };

  dispatch(action);
};
